﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameSpawner : MonoBehaviour
  
{

    public Rect spawnRect;
    int Flamecounter = 0;
    public float Timer = 0f;
    public int MaxFlames = 50;
    public FlameMove FlamePrefab;
    public float xMin, yMin;
    public float width, height;
    public int minFlamePeriod = 2;
    public int maxFlamePeriod = 10;

    public int flamePeriod = 2;


    void Start()
    {

        FlameMake();

    }

    public void FlameMake()
    {
        if (MaxFlames < 5)
        {
            MaxFlames = 20;
        }
       
        if (Flamecounter == Flamecounter)
        {
           
            FlameMove flame = Instantiate(FlamePrefab);

            flame.transform.parent = transform;

            

            flame.gameObject.name = "Flame " + Flamecounter;

           
            float x = spawnRect.xMin +
            Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            flame.transform.position = new Vector2(x, y);
           
            Flamecounter++;

        }

    }


   
    void Update()
    {
        if (Timer < Time.time)
        { 
            FlameMake();
            Timer = Time.time + (Random.Range(minFlamePeriod, maxFlamePeriod));
        }
    }


}
