using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSpawner : MonoBehaviour
{


    public Rect spawnRect;
    int Watercounter = 0;
    public float Timer = 0f;
    public int MaxWaters = 50;
    public WaterMove WaterPrefab;
    public float xMin, yMin;
    public float width, height;
    public int minWaterPeriod = 2;
    public int maxWaterPeriod = 10;

    public int waterPeriod = 2;


    void Start()
    {

        WaterMake();

    }

    public void WaterMake()
    {

        if (MaxWaters < 5)
        {
            MaxWaters = 20;
        }

        if (Watercounter == Watercounter)
        {
            
            WaterMove water = Instantiate(WaterPrefab);

            water.transform.parent = transform;

            

            water.gameObject.name = "Water " + Watercounter;

            

            float x = spawnRect.xMin +
            Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            water.transform.position = new Vector2(x, y);
           
            Watercounter++;

        }

    }


  

    void Update()
    {
        if (Timer < Time.time)
        { 
            WaterMake();
            Timer = Time.time + (Random.Range(minWaterPeriod,maxWaterPeriod));
        }
    }


}
