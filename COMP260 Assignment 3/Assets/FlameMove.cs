﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameMove : MonoBehaviour
{
  
    public float speed = 4.0f;
    public float turnSpeed = 180.0f;
    public Transform target;

    private int score;

    private Vector2 heading = Vector2.right;

    public AudioClip FlameSound;

    void Update()
    {

      
        target = target.transform;

      
        Vector2 direction = target.position - transform.position;

        
        float angle = turnSpeed * Time.deltaTime;
       
        if (direction.IsOnLeft(heading))
        {
            
            heading = heading.Rotate(angle);
        }
        else
        {
            
            heading = heading.Rotate(-angle);
        }
        transform.Translate(heading * speed * Time.deltaTime);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.name == "Player")
        {
             Destroy(gameObject);

        }

        if (col.collider.name == "Kill")
        {
            Destroy(gameObject);

        }

    }

}
