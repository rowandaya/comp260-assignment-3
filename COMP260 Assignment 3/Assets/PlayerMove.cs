
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PlayerMove : MonoBehaviour
{
    private int count;

    public Text countText;

    public float speed = 100;

    public Rigidbody2D rb;

    public Text healthText;

    public int health = 100;
    public Image HealthImage;

    public Text winningText;

    public AudioClip waterCollideClip;
    public AudioClip flameCollideClip;
    private AudioSource audio;





    void Start()
    {
        count = 0;
        SetCountText();
        SetHealthText();
        audio = GetComponent<AudioSource>();
    }



    public void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(h, v, 0);
        movement = movement.normalized * speed * Time.deltaTime;
        rb.MovePosition(rb.transform.position + movement);

  
        if (health == 0) {
           
            Time.timeScale = 0.0f;
            audio.pitch = Time.timeScale;
            winningText.text = "YOU DIED \n GAME OVER";
        }
       
            }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.tag == "Water")
        {
            count = count + 1;
            SetCountText();
            audio.PlayOneShot(waterCollideClip);

        }

        if (col.collider.tag == "Flame")
        {

            health = health - 20;
            SetHealthText();
            audio.PlayOneShot(flameCollideClip);

            HealthImage.fillAmount = HealthImage.fillAmount - 0.2f;

        }
    }


    void SetCountText()
    {
        countText.text = "Score: " + count.ToString();
    }
    void SetHealthText()
    {
        healthText.text = " " + health.ToString();
    }
    
}